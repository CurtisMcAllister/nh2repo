import datetime
import json

from flask import Flask, jsonify, make_response, request
from flask_cors import CORS
from pymongo import MongoClient, DESCENDING
from bson import json_util
import pandas_datareader as pdr


app = Flask(__name__)
CORS(app)
app.secret_key = 'kFbAYIQgh2rA'

client = MongoClient('mongodb://127.0.0.1:27017')
db = client.test
portfolio = db.portfolio


def add_new_record():
    current_date = datetime.datetime.now()

    if portfolio.count() == 0:
        new_record = {
            'cash': 0,
            'date': current_date,
            'stocks': [],
            'total_value': 0
        }
    else:
        previous_record = portfolio.find_one(sort=[('_id', DESCENDING)])

        # a range of dates to read current stock price due to yahoo finance inconsistencies
        price_start_date = current_date - datetime.timedelta(3)

        # update stock prices
        for stock in previous_record['stocks']:
            stock['price'] = pdr.get_data_yahoo(stock['ticker'], price_start_date)['Adj Close'][-1]

        new_record = {
            'cash': previous_record['cash'],
            'date': current_date,
            'stocks': previous_record['stocks'],
            'total_value': calculate_portfolio_value(previous_record['cash'], previous_record['stocks'])
        }

    new_record_id = portfolio.insert_one(new_record)
    new_record_url = 'http://localhost:5000/v1/api/portfolio/', str(new_record_id)
    return make_response(jsonify({'url': new_record_url}, 201))


def calculate_portfolio_value(current_cash, current_stocks):
    portfolio_value = current_cash

    start_date = datetime.datetime.now() - datetime.timedelta(3)

    for stock in current_stocks:
        current_price = pdr.get_data_yahoo(stock['ticker'], start_date)['Adj Close'][-1]
        stock_value = float(current_price) * int(stock['quantity'])
        portfolio_value += stock_value

    return portfolio_value


@app.route('/v1/api/portfolio/current', methods=['GET'])
def get_latest_record():
    data_to_return = portfolio.find_one(sort=[('_id', DESCENDING)])

    if data_to_return['date'].date() != datetime.datetime.now().date():
        add_new_record()
        data_to_return = portfolio.find_one(sort=[('_id', DESCENDING)])

    data_to_return['date'] = data_to_return['date'].date().__str__()

    json_data = json.loads(json_util.dumps(data_to_return))
    return make_response(jsonify(json_data), 200)


@app.route('/v1/api/portfolio/historical_values', methods=['GET'])
def get_historical_value():
    data_to_return = portfolio.find({}, {'_id': 0, 'date': 1, 'total_value': 1})

    values = []
    for record in data_to_return:
        values.append({'date': record['date'].date().__str__(), 'value': record['total_value']})

    json_data = json.loads(json_util.dumps(values))
    return make_response(jsonify(json_data), 200)


@app.route('/v1/api/portfolio/add_cash', methods=['PUT'])
def add_portfolio_cash():
    message = None
    result = None

    current_record = portfolio.find_one(sort=[('_id', DESCENDING)])

    if current_record['date'].date() != datetime.datetime.now().date():
        add_new_record()
        current_record = portfolio.find_one(sort=[('_id', DESCENDING)])

    if 'cash' in request.form:
        result = portfolio.update_one({'_id': current_record['_id']},
                                      {'$set': {'cash': current_record['cash'] + float(request.form['cash']),
                                                'date': datetime.datetime.now(),
                                                'stocks': current_record['stocks'],
                                                'total_value': current_record['total_value'] + float(request.form['cash'])
                                                }
                                       })
        message = 'Cash added'

        if result.matched_count == 1:
            return make_response(jsonify({'message': message}), 200)
        else:
            return make_response(jsonify({'error': 'No such portfolio record'}), 404)
    else:
        return make_response(jsonify({'error': 'Missing form data'}), 404)


@app.route('/v1/api/portfolio/remove_cash', methods=['PUT'])
def remove_portfolio_cash():
    message = None
    result = None

    current_record = portfolio.find_one(sort=[('_id', DESCENDING)])

    if current_record['date'].date() != datetime.datetime.now().date():
        add_new_record()
        current_record = portfolio.find_one(sort=[('_id', DESCENDING)])

    if 'cash' in request.form:
        if float(request.form['cash']) > current_record['cash']:
            message = 'Insufficient Cash to withdraw'
            return make_response(jsonify({'message': message}), 200)
        else:
            result = portfolio.update_one({'_id': current_record['_id']},
                                          {'$set': {'cash': current_record['cash'] - float(request.form['cash']),
                                                    'date': datetime.datetime.now(),
                                                    'stocks': current_record['stocks'],
                                                    'total_value': current_record['total_value'] - float(request.form['cash'])
                                                    }
                                           })
            message = 'Cash removed'

            if result.matched_count == 1:
                return make_response(jsonify({'message': message}), 200)
            else:
                return make_response(jsonify({'error': 'No such portfolio record'}), 404)
    else:
        return make_response(jsonify({'error': message}), 404)


@app.route('/v1/api/portfolio/stocks/buy_stock', methods=['PUT'])
def add_portfolio_stocks():
    message = None
    cost = 0

    current_record = portfolio.find_one(sort=[('_id', DESCENDING)])

    if current_record['date'].date() != datetime.datetime.now().date():
        add_new_record()
        current_record = portfolio.find_one(sort=[('_id', DESCENDING)])

    # a range of dates to read current stock price due to yahoo finance inconsistencies
    price_start_date = datetime.datetime.now() - datetime.timedelta(3)

    if 'ticker' in request.form and 'quantity' in request.form:
        added = False
        for stock in current_record['stocks']:
            if stock['ticker'].upper() == request.form['ticker'].upper():
                stock['quantity'] += int(request.form['quantity'])
                cost = stock['price'] * int(request.form['quantity'])
                added = True

        if not added:
            current_record['stocks'].append({
                'ticker': request.form['ticker'].upper(),
                'quantity': int(request.form['quantity']),
                'price': pdr.get_data_yahoo(request.form['ticker'], price_start_date)['Adj Close'][-1]
            })
            cost = int(request.form['quantity']) * int(current_record['stocks'][-1]['price'])

        if cost > current_record['cash']:
            message = 'Insufficient Funds to buy stocks'
            return make_response(jsonify({'error': message}), 404)
        else:
            result = portfolio.update_one({'_id': current_record['_id']},
                                          {'$set': {'cash': current_record['cash'] - cost,
                                                    'date': datetime.datetime.now(),
                                                    'stocks': current_record['stocks']
                                                    }
                                           })
            message = 'Transaction Successful'

            if result.matched_count == 1:
                return make_response(jsonify({'message': message}), 200)
            else:
                return make_response(jsonify({'message': 'No such portfolio record'}), 404)
    else:
        return make_response(jsonify({'message': 'Missing form data'}), 404)


@app.route('/v1/api/portfolio/stocks/sell_stock', methods=['PUT'])
def remove_portfolio_stocks():
    message = None
    cash_value = 0

    current_record = portfolio.find_one(sort=[('_id', DESCENDING)])

    if current_record['date'].date() != datetime.datetime.now().date():
        add_new_record()
        current_record = portfolio.find_one(sort=[('_id', DESCENDING)])

    if 'ticker' in request.form and 'quantity' in request.form:
        removed = False

        for stock in current_record['stocks']:
            if stock['ticker'].upper() == request.form['ticker'].upper():
                if stock['quantity'] < int(request.form['quantity']):
                    cash_value = stock['price'] * int(request.form['quantity'])
                    # message = 'Insufficient stocks: selling {} stocks for ${}'.format(stock['quantity'], cash_value)
                    message = 'Transaction Successful'
                    current_record['stocks'].remove(stock)
                else:
                    stock['quantity'] -= int(request.form['quantity'])
                    cash_value = stock['price'] * int(request.form['quantity'])
                    message = 'Transaction Successful'
                    removed = True
                    if stock['quantity'] == 0:
                        current_record['stocks'].remove(stock)

        if not removed and message is None:
            message = 'You do not have any of that stock'
            return make_response(jsonify({'message': 'No such portfolio record'}), 404)

        result = portfolio.update_one({'_id': current_record['_id']},
                                      {'$set': {'cash': current_record['cash'] + cash_value,
                                                'date': datetime.datetime.now(),
                                                'stocks': current_record['stocks']
                                                }
                                       })
        if result.matched_count == 1:
            return make_response(jsonify({'message': message}), 200)
        else:
            return make_response(jsonify({'message': 'No stocks to sell'}), 404)
    else:
        return make_response(jsonify({'message': 'Missing form data'}), 404)


if __name__ == "__main__":
    app.run(debug=True)


