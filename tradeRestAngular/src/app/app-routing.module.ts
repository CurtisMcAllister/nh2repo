import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { TradeComponent } from './trade/trade.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { InformationComponent } from './information/information.component';


const routes: Routes = [
  // we add root-relative routes here
  { path: '', component: TradeComponent },
  { path: 'trade', component: TradeComponent },
  { path: 'portfolio', component: PortfolioComponent },
  { path: 'information', component: InformationComponent },
  { path: 'trades/:id', component:TradeComponent}, // one param
  { path: '**', redirectTo:'trade' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

