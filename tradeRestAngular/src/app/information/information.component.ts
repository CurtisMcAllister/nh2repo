import { Component, OnInit } from '@angular/core';
import { FinService } from '../services/fin.service';
import { ChartDataSets } from 'chart.js';
import { Label, Color } from 'ng2-charts';
import { TradeService } from '../services/trade.service';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit {
  // model variables
  ticker:string = 'C';
  days:number = 10;
  data;
  advice;
  current_price;

  graphLoaded:boolean = false;

  lineChartData: ChartDataSets[] = [
    { data: null, label: 'Stock Price(USD)' },
  ];

  lineChartLabels: Label[] = [];

  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(0,255,0,0.28)',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';

  constructor(private myFinService:FinService) { }

  ngOnInit(): void {
    this.getDataFromTicker();
    this.getAdvice();
    this.getCurrentPrice();
  }

  getCurrentPrice(){
    this.myFinService.getPrice(this.ticker).subscribe((data)=>{
      this.current_price = data['price'];
    })
  }

  getDataFromTicker(){
      this.myFinService.getStockPrice(this.ticker, this.days).subscribe( (response_data) =>{
        this.data = response_data;
        this.lineChartLabels = this.data.prices.map(item => item['date']);
        
        var d1 = this.data.prices.map(item => item['price']);

        this.lineChartData[0]['data'] = d1;

        this.graphLoaded = true;
      })
      this.getCurrentPrice();
  }

  getAdvice(){
    this.myFinService.getTradeAdvice(this.ticker, this.days)
      // we subscribe to the observable, with a call-back function
      .subscribe( (data) =>{ 
        console.log(data);
        this.advice = data['advice'];
       })
  }

}
