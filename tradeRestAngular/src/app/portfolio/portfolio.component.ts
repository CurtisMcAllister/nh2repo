import { Component, OnInit } from '@angular/core';
import { PortfolioService } from '../services/portfolio.service';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {
  current_portfolio;
  historical_values;
  add_amount:number;
  remove_amount:number;

  valid_add:boolean = true;
  valid_remove:boolean = true;
  graphLoaded:boolean = false;

  lineChartData: ChartDataSets[] = [
    { data: null, label: 'Portfolio Value(USD)' },
  ];

  lineChartLabels: Label[] = [];

  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(0,255,0,0.28)',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';

  constructor(private portfolioService:PortfolioService) { }

  ngOnInit(): void {
    this.getCurrentPortfolio()
    this.getHistoricalValues()
  }

  getCurrentPortfolio(){
    this.portfolioService.getCurrentPortfolio()
      .subscribe( (response_data) =>{ this.current_portfolio = response_data });
  }

  getHistoricalValues(){
    this.portfolioService.getHistoricalValues()
      .subscribe( (response_data) =>{
        this.historical_values = response_data;
        this.lineChartLabels = this.historical_values.map(item => item['date']);

        var d1 = this.historical_values.map(item => item['value']);

        this.lineChartData[0]['data'] = d1;

        this.graphLoaded = true;
      })
  }

  addCash(){
    if(isNaN(this.add_amount)){
      console.log(isNaN(this.add_amount))
      this.valid_add = false;
      
    }
    else{
      this.portfolioService.putAddCash(this.add_amount);
      this.valid_add = true;
    }
  }
  
  removeCash(){
    if(isNaN(this.remove_amount)){
      this.valid_remove = false;
      
    }
    else{
      this.portfolioService.putRemoveCash(this.remove_amount);
      this.valid_remove = true;
    }
  }
}
