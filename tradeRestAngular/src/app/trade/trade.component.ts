import { Component, OnInit } from '@angular/core';
import { TradeService } from '../services/trade.service';
import { PortfolioService } from '../services/portfolio.service';

@Component({
  selector: 'app-trade',
  templateUrl: './trade.component.html',
  styleUrls: ['./trade.component.css']
})
export class TradeComponent implements OnInit {
  data;
  tickercode='MSFT';
  num=1;

  trade_message;

  constructor(private myTradeService:TradeService,
              private myPortfolioService:PortfolioService) { }
    // button handler method
    getTradesFromAPI(){
      // console.log('get from code')
      this.myTradeService.getTrades()
      .subscribe( (response)=>{this.data = response} )
    }

  ngOnInit(): void {
    this.getTradesFromAPI()
  }

  createTrade(action){
    this.myTradeService.postTrade(this.tickercode.toUpperCase(), this.num, action);
  }

  createBuy(){
    this.myPortfolioService.putBuyStocks(this.tickercode, this.num).subscribe((data)=>{
      this.trade_message = data['message']
      console.log(data)
    })
    if(this.trade_message == 'Transaction Successful'){
      this.trade_message = null;
    }
    this.createTrade('BUY');
  }

  createSell(){
    this.myPortfolioService.putSellStocks(this.tickercode, this.num).subscribe((data)=>{
      this.trade_message = data['message']
    });
    if(this.trade_message == 'Transaction Successful'){
      console.log
      this.trade_message = null;
    }
    this.createTrade('SELL');
  }
}
