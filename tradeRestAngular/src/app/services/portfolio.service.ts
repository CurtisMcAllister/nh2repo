import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PortfolioService {
  basePortfolioUrl = 'http://localhost:5000/v1/api/portfolio/'

  constructor(private http:HttpClient) { }

  getCurrentPortfolio(){
    let url = `${this.basePortfolioUrl}current`;

    return this.http.get(url);
  }

  getHistoricalValues(){
    let url = `${this.basePortfolioUrl}historical_values`;
    return this.http.get(url);
  }

  putAddCash(amount){
    let url = `${this.basePortfolioUrl}add_cash`;
    var putData:any = new FormData()

    putData.append("cash", amount);

    return this.http.put(url, putData).subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
    );
  }

  putRemoveCash(amount){
    let url = `${this.basePortfolioUrl}remove_cash`;
    var putData:any = new FormData()

    putData.append("cash", amount);

    return this.http.put(url, putData)
  }

  putBuyStocks(ticker, quantity){
    let url = `${this.basePortfolioUrl}stocks/buy_stock`;
    var putData:any = new FormData();

    putData.append("ticker", ticker);
    putData.append("quantity", quantity);

    return this.http.put(url, putData)
  }

  putSellStocks(ticker, quantity){
    let url = `${this.basePortfolioUrl}stocks/sell_stock`;
    var putData:any = new FormData();

    putData.append("ticker", ticker);
    putData.append("quantity", quantity);

    return this.http.put(url, putData)
  }

}
