import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FinService {
  baseInfoUrl = 'https://o5oxywg6r3.execute-api.eu-west-1.amazonaws.com/default/nh2-price-api/?ticker='
  baseAdviceUrl = 'https://4xd5dm4w6a.execute-api.eu-west-1.amazonaws.com/default/nh2-advice-api/?ticker='
  basePriceUrl = 'https://hjxwcw6pr6.execute-api.eu-west-1.amazonaws.com/default/nh2-currentPrice/?ticker='
  
  constructor(private http:HttpClient) {}

  getStockPrice(ticker:string, days:number){
    let fullURL = `${this.baseInfoUrl}${ticker}&num_days=${days}`
    return this.http.get(fullURL)
  }

  getTradeAdvice(ticker:string, window:number){
    let fullURL = `${this.baseAdviceUrl}${ticker}&window=${window}`
    return this.http.get(fullURL)
  }

  getPrice(ticker){
    let url = `${this.basePriceUrl}${ticker}`;
    return this.http.get(url);
  }
}
