import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TradeService {
  baseInfoUrl = 'http://localhost:8080/api/trade'
  basePriceUrl = 'https://hjxwcw6pr6.execute-api.eu-west-1.amazonaws.com/default/nh2-currentPrice/?ticker='
  current_price;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private http:HttpClient) { }

  getTrades(){
    return this.http.get(this.baseInfoUrl);
  }

  setPrice(ticker){
    let url = `${this.basePriceUrl}${ticker}`;
    this.http.get(url).subscribe((data)=>{this.current_price = data['price']});
  }

  postTrade(ticker, quantity, action){
    this.setPrice(ticker);

    var httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })};

    if(isNaN(this.current_price)){
      
    }
    else{
      let trade = {};
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();
      var today_str = yyyy + '-' + mm + '-' + dd;

      trade['date'] = today_str;
      trade['ticker'] = ticker;
      trade['quantity'] = quantity;
      trade['price'] = this.current_price;
      trade['state'] = 'CREATED';
      trade['action'] = action;

      console.log(trade)

      return this.http.post(this.baseInfoUrl, JSON.stringify(trade), httpOptions).subscribe((data)=>{console.log(data)});
    }
    
  }

}
