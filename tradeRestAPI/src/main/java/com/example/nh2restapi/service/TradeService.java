package com.example.nh2restapi.service;

import java.util.Collection;
import java.util.Optional;

import com.example.nh2restapi.entities.Trade;

import org.bson.types.ObjectId;

public interface TradeService {
    
    Collection<Trade> getTrades();

    Optional<Trade> getTradeById(ObjectId id);

    void addTrade(Trade t);

    void removeTradeById(ObjectId id);

    void updateTrade(Trade trade);

}