package com.example.nh2restapi.service;

import java.util.Collection;
import java.util.Optional;

import com.example.nh2restapi.entities.Trade;
import com.example.nh2restapi.repo.TradeRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TradeServiceImpl implements TradeService {

    @Autowired
    private TradeRepository repo;

    @Override
    public Collection<Trade> getTrades() {
        return repo.findAll();
    }

    @Override
    public Optional<Trade> getTradeById(ObjectId id) {
        return repo.findById(id);
    }

    @Override
    public void addTrade(Trade t) {
        repo.insert(t);

    }

    @Override
    public void removeTradeById(ObjectId id) {
        repo.deleteById(id);
    }

    @Override
    public void updateTrade(Trade trade) {
        repo.save(trade);
    }
}