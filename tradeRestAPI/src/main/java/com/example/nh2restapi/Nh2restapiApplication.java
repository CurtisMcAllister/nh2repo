package com.example.nh2restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Nh2restapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Nh2restapiApplication.class, args);
	}

}
