package com.example.nh2restapi.rest;

import java.util.Collection;
import java.util.Optional;

import com.example.nh2restapi.entities.Trade;
import com.example.nh2restapi.service.TradeService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("api/trade")
public class TradeController {
    
    @Autowired
    private TradeService tradeService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Trade> getTrades(){
        return tradeService.getTrades();
    }

    @RequestMapping(method=RequestMethod.GET, value="/{id}")
    public Optional<Trade> getTradeById(@PathVariable("id") String id) {
		return tradeService.getTradeById(new ObjectId(id));
	}

    @RequestMapping(method = RequestMethod.POST)
    public void addTrade(@RequestBody Trade t){
        tradeService.addTrade(t);
    }

    @RequestMapping(method=RequestMethod.DELETE, value="/{id}")
    public @ResponseBody void removeTradeById(@PathVariable("id") String id) {
		tradeService.removeTradeById(new ObjectId(id));
    }

    @RequestMapping(method=RequestMethod.PUT)
    public @ResponseBody void updateTrade(@RequestBody Trade trade){
        tradeService.updateTrade(trade);
    }

}