package com.example.nh2restapi.entities;

public enum TradeState {
    CREATED, PENDING, CANCELLED, REJECTED, FILLED, PARTIALLY_FILLED, ERROR, PROCESSING
   
}