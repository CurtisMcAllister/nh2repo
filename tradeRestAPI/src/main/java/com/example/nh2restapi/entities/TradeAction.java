package com.example.nh2restapi.entities;

public enum TradeAction {
    BUY, SELL
}
