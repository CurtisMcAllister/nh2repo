package com.example.nh2restapi.entities;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
public class Trade {
    
    @Id
    private ObjectId id;
    private Date date;
    private String ticker;
    private int quantity;
    private double price;
    private TradeState state;
    private TradeAction action;


    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public TradeState getState() {
        return state;
    }

    public void setState(TradeState state) {
        this.state = state;
    }

    public TradeAction getAction() {
        return action;
    }

    public void setAction(TradeAction action) {
        this.action = action;
    }
    

    public Trade(Date date, String ticker, int quantity, double price, TradeState state) {
        this.date = date;
        this.ticker = ticker;
        this.quantity = quantity;
        this.price = price;
        this.state = state;
    }

    public Trade(){}



}