package com.example.nh2restapi.repo;

import com.example.nh2restapi.entities.Trade;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeRepository extends MongoRepository<Trade, ObjectId> {
    
}