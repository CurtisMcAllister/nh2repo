package com.example.nh2restapi;

import com.example.nh2restapi.rest.TradeController;
import com.example.nh2restapi.entities.Trade;
import com.example.nh2restapi.entities.TradeState;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;


@SpringBootTest
class Nh2restapiApplicationTests {

	@Autowired
	private TradeController controller;


	// Simple sanity check test that will fail if the application context cannot start
	@Test
	void contextLoads() {
	}

	// Simple sanity check to test that the context creates the controller
	@Test
	public void contextLoads1() throws Exception {
		assertThat(controller).isNotNull();
	}

	// Testing basic trade functionality
	// Testing for null values/negative values/empty values will take place on front end
	@Test
    public void testSetQuantity() {
        Trade trade1 = new Trade();
        trade1.setQuantity(30);
        assertTrue(trade1.getQuantity() == 30);
	}
	
	@Test
    public void testSetPrice() {
        Trade trade2 = new Trade();
        trade2.setPrice(450);
        assertTrue(trade2.getPrice() == 450);
    }

	@Test
    public void testSetTicker() {
        Trade trade3 = new Trade();
        trade3.setTicker("MSFT");
        assertTrue(trade3.getTicker() == "MSFT");
	}

	@Test
	public void testTradeConstructor() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date myDate = sdf.parse("2020-09-02");
		Trade testTrade = new Trade(myDate, "MSFT", 10, 55.5, TradeState.CREATED);
		assertTrue(testTrade.getDate() == myDate);
		assertTrue(testTrade.getTicker() == "MSFT");
		assertTrue(testTrade.getQuantity() == 10);
		assertTrue(testTrade.getPrice() == 55.5);
		assertTrue(testTrade.getState() == TradeState.CREATED);
	}

	@Test
    public void testSetDate() throws ParseException {
    	Trade tradeDate = new Trade();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date myDate = sdf.parse("2020-09-02");
    	tradeDate.setDate(myDate);
    	assertTrue(tradeDate.getDate() == myDate);
	}

	@Test
    public void testSetState() {
        Trade tradeState = new Trade();
        tradeState.setState(TradeState.CREATED);
        assertTrue(tradeState.getState() == TradeState.CREATED);
	}

	@Test
    public void testSetID() {
		Trade tradeID = new Trade();
		ObjectId obj = new ObjectId();
        tradeID.setId(obj);
        assertTrue(tradeID.getId() == obj);
	}
}