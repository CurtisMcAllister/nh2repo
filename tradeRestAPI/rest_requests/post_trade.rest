POST http://localhost:8080/api/trade HTTP/1.1
content-type: application/json

{
    "date": "2020-09-17",
    "ticker": "MSFT",
    "quantity": 10,
    "price": 200.91,
    "state": "CREATED",
    "action": "SELL"
}