import datetime
import json

import pandas_datareader as pdr
from pandas_datareader._utils import RemoteDataError


def lambda_handler(event, context):
    ticker = ''

    # initialise ticker from query string parameters
    if event['queryStringParameters'] and 'ticker' in event['queryStringParameters']:
        ticker = event['queryStringParameters']['ticker']

    # set starting data for data collection
    start_date = datetime.datetime.now() - datetime.timedelta(1)

    # get data for ticker from yahoo finance
    try:
        current_price = pdr.get_data_yahoo(ticker, start_date)['Close'][-1]
    except RemoteDataError as ex:
        return {'statusCode': 404, 'body': json.dumps({'msg': 'ticker not found',
                                                       'ticker': ticker,
                                                       'error': ex})}

    return {
        'statusCode': 200,
        'body': json.dumps({'ticker': ticker,
                            'price': current_price})
    }
