import datetime
import json

import pandas_datareader as pdr
from pandas_datareader._utils import RemoteDataError


def lambda_handler(event, context):
    ticker = ''
    window = 0

    # get ticker and window from query strings
    if event['queryStringParameters'] and 'ticker' in event['queryStringParameters']:
        ticker = event['queryStringParameters']['ticker']

    if event['queryStringParameters'] and 'window' in event['queryStringParameters']:
        window = event['queryStringParameters']['window']

    # set starting date using window
    start_date = datetime.datetime.now() - datetime.timedelta(window)

    # get ticker data from yahoo finance
    try:
        df = pdr.get_data_yahoo(ticker, start_date)
    except RemoteDataError as ex:
        return {'statusCode': 404, 'body': json.dumps({'msg': 'ticker not found',
                                                       'ticker': ticker,
                                                       'error': ex})}

    # calculate bollinger bands
    df['window mavg'] = df['Close'].rolling(window=window).mean()
    df['window std'] = df['Close'].rolling(window=window).std()

    df['Upper Band'] = df['window mavg'] + (df['window std'] * 2)
    df['Lower Band'] = df['window mavg'] - (df['window std'] * 2)

    # determine advice from bollinger bands
    if df['Close'].iloc[-1] > df['Upper Band'].iloc[-1]:
        response = 'Sell'
    elif df['Close'].iloc[-1] < df['Lower Band'].iloc[-1]:
        response = 'Buy'
    else:
        response = 'Hold'

    return {
        'statusCode': 200,
        'body': json.dumps({'ticker': ticker,
                            'price': df['Close'][-1],
                            'advice': response
                            })
    }
