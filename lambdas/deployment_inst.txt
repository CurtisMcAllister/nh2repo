# zip a lambda_function
tar.exe -a -c -f {zip-name}.zip lambda_function.py

# deploy to aws
aws lambda update-function-code --function-name {func-name} --zip-file fileb:{file-name}
